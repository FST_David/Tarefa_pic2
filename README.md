# Projeto de Recrutamento - PIC_2

## Descrição

### Programa básico

Na primeira parte da tarefa deverás construir um programa que monitoriza uma tensão, que terá as seguintes funcionalidades:

+ Fazer uma medição de tensão a cada 10 ms utilizando o ADC do PIC.
+ LED Azul - Está ON quando a tensão é menor que 1V durante mais de 1 segundos. Está OFF caso contrário.
+ LED Vermelho - Está ON quando a tensão à entrada do ADC é maior que 2.5V. Está OFF quando a tensão é menor que 2.5V durante mais de 3 segundos.
+ LED Verde - Está ON quando o LED_Vermelho está OFF e a tensão é menor que 2.5V. Está OFF caso contrário.


### Novas funcionalidades

Depois de teres um programa básico irás acrescentar-lhe 3 novas funcionalidades, mas deverás trabalhar em cada uma das funcionalidades de maneira isolada sempre partindo do programa básico. No final quando as 3 funcionalidades estiverem prontas deverás juntá-las no programa final. As funcionalidades são as seguintes.

1. Um segundo canal de monitorização com as mesmas propriedades do primeiro e totalmente independente deste.
2. Se a tensão é superior a 2.5V o LED vermelho deverá ligar e ficar sempre ligado até que se carregue num botão. Mesmo que a tensão baixe de 2.5V.
3. Existe um quarto LED de cor arbitrária cuja intensidade deverá ser regulada proporcionalmente à tensão medida. Para isto deverás usar PWM que é implementado nos nossos PIC com o módulo de Output Compare e não com o módulo de High Speed PWM.