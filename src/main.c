// FST Lisboa
// Project Template

// DSPIC33EP256MU806 Configuration Bit Settings

// 'C' source line config statements

// FGS
#pragma config GWRP = OFF				// General Segment Write-Protect bit (General Segment may be written)
#pragma config GSS = OFF				// General Segment Code-Protect bit (General Segment Code protect is disabled)
#pragma config GSSK = OFF				// General Segment Key bits (General Segment Write Protection and Code Protection is Disabled)

// FOSCSEL
#pragma config FNOSC = FRC				// Initial Oscillator Source Selection bits (Internal Fast RC (FRC))
#pragma config IESO = OFF				// Two-speed Oscillator Start-up Enable bit (Start up with user-selected oscillator source)

// FOSC
#pragma config POSCMD = HS				// Primary Oscillator Mode Select bits (XT Crystal Oscillator Mode)
#pragma config OSCIOFNC = OFF			// OSC2 Pin Function bit (OSC2 is clock output)
#pragma config IOL1WAY = OFF			// Peripheral pin select configuration (Allow_A multiple reconfigurations)
#pragma config FCKSM = CSECMD			// Clock Switching Mode bits (Clock switching is enabled,Fail-safe Clock Monitor is disabled)

// FWDT
#pragma config WDTPOST = PS32768		// Watchdog Timer Postscaler bits (1:32,768)
#pragma config WDTPRE = PR128			// Watchdog Timer Prescaler bit (1:128)
#pragma config PLLKEN = ON				// PLL Lock Wait Enable bit (Clock switch to PLL source will wait until the PLL lock signal is valid.)
#pragma config WINDIS = OFF				// Watchdog Timer Window Enable bit (Watchdog Timer in Non-Window mode)
#pragma config FWDTEN = ON				// Watchdog Timer Enable bit (Watchdog timer always enabled)

// FPOR
#pragma config FPWRT = PWR4		        // Power-on Reset Timer Value Select bits (4s)
#pragma config BOREN = ON				// Brown-out Reset (BOR) Detection Enable bit (BOR is enabled)
#pragma config ALTI2C1 = OFF			// Alternate I2C pins for I2C1 (SDA1/SCK1 pins are selected as the I/O pins for I2C1)

// FICD
#pragma config ICS = NONE				// ICD Communication Channel Select bits (Reserved, do not use)
#pragma config RSTPRI = PF				// Reset Target Vector Select bit (Device will obtain reset instruction from Primary flash)
#pragma config JTAGEN = OFF				// JTAG Enable bit (JTAG is disabled)

// FAS
#pragma config AWRP = OFF				// Auxiliary Segment Write-protect bit (Aux Flash may be written)
#pragma config APL = OFF				// Auxiliary Segment Code-protect bit (Aux Flash Code protect is disabled)
#pragma config APLK = OFF				// Auxiliary Segment Key bits (Aux Flash Write Protection and Code Protection is Disabled)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <p33EP256MU806.h>
#include "lib_pic33e/timing.h"  // NOTE: Always include timing.h
#include "lib_pic33e/usb_lib.h" //Comment this line if USB is not needed

#define LED_VERDE               LATEbits.LATE3

// Global declarations
int button;
int counter_blue_A;
int counter_green_B;
int counter_blue_B;
int safe_A = 1;       // Inicialized on GREEN;
int safe_B = 1;       // Inicialized on GREEN wire;
int low_A = 0;        // Inicialized on GREEN (clear blue);
int low_B = 0;        // Inicialized on GREEn wire (clear blue light);
void my_adc_configuration(void);
void my_PWM_configuration(void);

int main(){
    // Configurations
    my_adc_configuration ();
    my_PWM_configuration();    

    // Local variable declaration
    int button_local;
    int safe_local_A;
    int safe_local_B;
    int low_local_A;
    int low_local_B;

    USBInit();

    while(1)
    {
        low_local_A = low_A;
        safe_local_A = safe_A;
        safe_local_B = safe_B;
        low_local_B = low_B;
        USBTasks();
        
        if ((PORTDbits.RD4 == 1) && (safe_local_A == 1)){
            button = 1;
        }

        button_local = button;

        //Clear watchdog timer
        ClrWdt();

        //Estados do primeiro canal CH0:
        if((button_local==0 && low_local_A==0) || (button_local==0 && low_local_A==1)){
            LATEbits.LATE5 = 1;     // Turn on RED;
            LATEbits.LATE3 = 0;     // Turn off GREEN;
            LATBbits.LATB12 = 0;    // Turn off solitário;
        }
        if((button_local==1) && (low_local_A==0)){
            LATEbits.LATE5 = 0;     // Turn off RED;
            LATEbits.LATE3 = 1;     // Turn on GREEN;
            LATBbits.LATB12 = 0;    // Turn off solitário;
        }
        if((button_local==1) && (low_local_A==1)){
            LATEbits.LATE5 = 0;     // Turn off RED;
            LATEbits.LATE3 = 0;     // Turn off GREEN;
            LATBbits.LATB12 = 1;    // Turn on solitário;
        }

        //Estados do segundo canal CH1:
        if((safe_local_B==0 && low_local_B==0) || (safe_local_B==0 && low_local_B==1)){
            LATBbits.LATB4 = 0;    // Turn off GREEN;
            LATBbits.LATB6 = 0;    // Turn off BLUE;
            LATBbits.LATB8 = 1;    // Turn on RED;
        }
        if((safe_local_B==1) && (low_local_B==0)){
            LATBbits.LATB4 = 1;    // Turn on GREEN;
            LATBbits.LATB6 = 0;    // Turn off BLUE;
            LATBbits.LATB8 = 0;    // Turn off RED;
        }
        if((safe_local_B==1) && (low_local_B==1)){
            LATBbits.LATB4 = 0;    // Turn off GREEN;
            LATBbits.LATB6 = 1;    // Turn on BLUE;
            LATBbits.LATB8 = 0;    // Turn off RED;
        }

    }//end while
    return 0;
}//end main


//Interrupt ADC
void adc_callback(void)
{
    IFS0bits.AD1IF = 0;         // Reset interrupt ocurrence;

    // Transições do CH0:
    if(ADC1BUF0>=775){
        safe_A = 0;             // Is it really bellow 2,5V even if button is pressed? No.
        low_A = 0;
        button = 0;
        counter_blue_A = 0;
    } else if((ADC1BUF0<775) && (ADC1BUF0>=310)){
        safe_A = 1;
        low_A = 0;
        counter_blue_A = 0;
    } else if (ADC1BUF0<310 && safe_A==0){          // If it has not been touched no 1sec delay
        safe_A = 1;
        low_A = 1;
    } else if (ADC1BUF0<310 && safe_A==1){
        counter_blue_A += 1;
    }

    if(counter_blue_A>=100){
        low_A =1;
        counter_blue_A=101;   // Don't let the buffer overflow_A
    }
    

    // Transições do CH1:
    if(ADC1BUF1>=775){
        safe_B = 0;
        low_B = 0;
        counter_blue_B = 0;
        counter_green_B = 0;
    } else if((ADC1BUF1<775) && (ADC1BUF1>=310)){
        low_B = 0;
        counter_blue_B = 0;
        counter_green_B += 1;
    } else if (ADC1BUF1<310){
        counter_blue_B += 1;
        counter_green_B += 1;
    }

    if(counter_green_B>=300){
        safe_B=1;
        counter_green_B=300;  // Don't let the buffer overflow
    }
    if(counter_blue_B>=100){
        low_B = 1;
    }
    if(counter_blue_B>=300){
        safe_B = 1;
        low_B = 1;
        counter_blue_B=300;   // Don't let the buffer overflow
    }

    OC1R = ((ADC1BUF0)/1023.0)*19999.0;   // Change duty cycle for PWM;
    
    // Print every second the 4 previous readings of CH0 and CH1;
    uprintf("\nVoltage_A = %lf\nVoltage_B = %lf",(ADC1BUF0/1023.0)*3.3,(ADC1BUF1/1023.0)*3.3);    
    return;
}

//Configuration: adc interrupting every 10 ms using timer3 to set the period
void my_adc_configuration (void)
{
    //I/O Configuration
    TRISEbits.TRISE3 = 0;   // Green led;
    TRISEbits.TRISE5 = 0;   // Red led;
    TRISEbits.TRISE7 = 0;   // Blue led;
    TRISBbits.TRISB12 = 0;  // Lonely led;
    LATEbits.LATE3 = 0;     // Turn off;
    LATEbits.LATE5 = 0;     // Turn off;
    LATEbits.LATE7 = 0;     // Turn off;
    LATBbits.LATB12 = 0;    // Turn off;

    TRISBbits.TRISB4 = 0;   // Green wire outputs;
    TRISBbits.TRISB6 = 0;   // Blue wire outputs;
    TRISBbits.TRISB8 = 0;   // Red wire outputs;
    LATBbits.LATB4 = 0;     // Turn off;
    LATBbits.LATB6 = 0;     // Turn off;
    LATBbits.LATB8 = 0;     // Turn off;

    TRISBbits.TRISB0 = 1;   // Voltage input for CH0;
    TRISBbits.TRISB3 = 1;   // Voltage input for CH1;
    TRISDbits.TRISD4 = 1;   // Input for button;

    //Timer3 configuration
    TMR3 = 0x0000;
    PR3 = 2499;            // Trigger ADC1 every 10 milisec;
    T3CONbits.TCKPS = 0b10; // Prescaler 1:64;
    IFS0bits.T3IF = 0;      // Clear Timer 3 interrupt flag;
    IEC0bits.T3IE = 0;      // Disable Timer 3 interrupt;

    //ADC configuration
    ANSELBbits.ANSB0 = 1;     // Make sure that PORTB AN0 is analog;
    ANSELBbits.ANSB3 = 1;     // Make sure that PORTB AN3 is analog;

    AD1CON1 = 0;              
    AD1CON1bits.SSRC = 0b010; // Timer3 trigger;
    AD1CON1bits.AD12B = 0;    // 10 bit mode in order to use 2 channels;
    AD1CON1bits.ASAM = 1;     // Auto sampling;
    AD1CON1bits.SIMSAM = 0;   // Sequential sampling
    
    AD1CON2 = 0;              // Generates interrupt after completion of 1 sample/convert operations;
    AD1CON2bits.CSCNA = 1;    // Scan Input Selections for CH0+ during Sample A bit;
    AD1CON2bits.CHPS = 0b01;  // Convert CH0 and CH1;
    AD1CON2bits.SMPI = 1;     // Interrupt every 2th sampling/conversion cycle;
    
    AD1CON3 = 0;              // Clock derived from system clock;
    AD1CON3bits.ADCS = 23;    // ADC conversion clock factor Tad = Tcy*24 = 1,5us;
    
    AD1CON4 = 0;              // DMA not enabled;
    
    AD1CHS123bits.CH123NA = 0;// CH1 negative input is Vrefl;
    AD1CHS123bits.CH123SA = 1;// CH1 positive input is AN3;
    AD1CHS0 = 0;              // CH0SA(positive input) ignored because I use channel scan / CH0NA(negative input) is Vref-
    AD1CSSH = 0;              // Not scanning ports AN16-AN31
    AD1CSSL = 0x0001;         // AN0 selected for scanning


    IFS0bits.AD1IF = 0;     // Clear the A/D interrupt flag bit;
    IEC0bits.AD1IE = 1;     // Enable A/D interrupt;
    IPC3bits.AD1IP = 6;     // ADC interrupt priority;

    AD1CON1bits.DONE = 0;   // Make sure DONE is 0 so that ASAM triggers sampling
    T3CONbits.TON = 1;      // Timer 3 module operating;
    AD1CON1bits.ADON = 1;   // ADC1 module is operating;
}

void my_PWM_configuration (void)
{
    // I/O configuration
    __builtin_write_OSCCONL(OSCCON & ~(1<<6));  // Unlock PPS Registers
    RPOR1bits.RP66R = 16;                       // PortD2
    __builtin_write_OSCCONL(OSCCON | (1<<6));   // Lock PPS Registers

    // Timer2 configuration;
    T2CON = 0;      
    T2CONbits.TCKPS = 1;    // 1:8 prescaler; 
    TMR2 = 0;
    //PR2 = 19999;          // Este registo é ignorado no modo de PWM
    IFS0bits.T2IF = 0;      // Clear Timer 2 interrupt flag;
    IEC0bits.T2IE = 0;      // Disable Timer 2 interrupt;
    
    // Output Compare configuration;
    OC1CON1 = 0;
    OC1CON1bits.OCSIDL = 0;     // No idle mode;
    OC1CON1bits.OCTSEL = 0;     // Timer2 as the clock;
    OC1CON1bits.OCM = 0b110;    // Edge aligned PWM mode;
    OC1CON2 = 0;
    OC1CON2bits.SYNCSEL = 31,   // Sync using OC1RS
    OC1RS = 19999;
    OC1R = 0;

    // Interrupts
    IFS0bits.OC1IF = 0;
    IEC0bits.OC1IE = 0;

    // Start timer
    T2CONbits.TON = 1;
}